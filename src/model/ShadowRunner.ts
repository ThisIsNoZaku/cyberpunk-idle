/**
 * A character that a player takes on 'Runs.
 */
import SkillDefinition from "./SkillDefinition";

export default class Shadowrunner {
    private _name: string;
    /**
     * The skills that the character has.
     */
    private _skills: Map<SkillDefinition, Number>;

    get name(): string {
        return this._name;
    }

    get skills(): Map<SkillDefinition, Number>{
        return this._skills;
    }
}