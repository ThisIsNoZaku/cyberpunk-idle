/**
 * Immutable template definition for a Mission.
 *
 * A mission is a major story mission.
 *
 * Missions are composed of one or more Challenges. Completing the challenges is earns the mission rewards for the team.
 */
import Challenge from "./Challenge";

export default class MissionDefinition {
    /**
     * The challenges that must be completed to finish this MissionDefinition.
     */
    private _challenges: Challenge[] ;
    /**
     * If this mission can be automatically repeated.
     *
     * Used for low-level side jobs that characters can use
     */
    private _repeatable: boolean;

    get challenges(): Challenge[] {
        return this._challenges;
    }

    get repeatable(): boolean {
        return this._repeatable;
    }
}