/**
 * A skill character can use to overcome challenges in missions and earn nuyen in side jobs.
 */
export default class SkillDefinition {
    private _name:string;

    get name():string{
        return this._name;
    }
}