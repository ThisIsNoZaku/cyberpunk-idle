import MissionDefinition from "./MissionDefinitioin";

export class MissionProgress {
    private _missionDefinition:MissionDefinition;
    /**
     * The index of the challenge which is currently in progress.
     */
    private _currentChallenge:number;
    /**
     * The amount of time, in seconds, remaining until the current challenge is completed.
     */
    private _remainingTime:number;

    get missionDefinition():MissionDefinition{
        return this._missionDefinition;
    }

    get currentChallengeIndex():number {
        return this._currentChallenge;
    }

    get currentChallenge():MissionDefinition{
        return this._missionDefinition.challenges()[this._currentChallenge];
    }

    get remainingTime():number{
        return this._remainingTime;
    }
}