/**
 *
 */
import SkillDefinition from "./SkillDefinition";

export default class Challenge {
    /**
     * Description shown to the player about this challenge.
     */
    private _description:string;
    /**
     * The minimum number of characters needed to complete the Challenge. If not this many characters are present, the
     * challenge automatically fails.
     */
    private _minimumPerformers:number;
    /**
     * The maximum number of characters that can attempt to complete the Challenge.
     */
    private _maximumPerformers:number;
    /**
     * The skills and skill levels required to complete the Challenge.
     */
    private _requiredSkillLevel:Map<SkillDefinition, number>;
    /**
     * The amount of time required to complete the mission, in seconds.
     */
    private _timeRequired:number;
}